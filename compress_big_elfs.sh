#!/bin/bash

# Compress ELFs in /usr/bin that are bigger than 20MB and not already compressed

fd . /usr/bin -S +20m -x file {} | awk -F ":" '/dynamically linked/{print $1}' | parallel-rust -j$(nproc) upx

