#!/usr/bin/env sh

# Take screenshot with scrot and upload it to imgur.
# Paste resulting imgur url to primary clipboard.
# Paste resulting delete image imgur url to /tmp/.imgur_del

OUT=$(mktemp -u "/tmp/.XXXXXX.png")
scrot $@ "$OUT"
optipng "$OUT"

URLS=$(imgur.sh "$OUT")

VIEW_URL=$(echo "$URLS" | head -n 1)
DEL_URL=$(echo "$URLS" | tail -n 1 | awk '{print $3}')

notify-send -t 2000 "Image uploaded: $VIEW_URL" &
echo "$VIEW_URL" | xclip -i
echo "$(date)" "$DEL_URL" >> /tmp/.imgur_del
rm "$OUT"
